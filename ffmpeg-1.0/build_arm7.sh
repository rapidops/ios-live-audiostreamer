#!/bin/bash
make clean
rm -rf compiled/armv7
mkdir compiled/armv7

export CC=/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/arm-apple-darwin10-llvm-gcc-4.2
export CFLAGS="-arch armv7 -pipe -miphoneos-version-min=5.0 -isysroot /Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS5.0.sdk -I/usr/local/include -I/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS5.0.sdk/usr/include"
export CPP=/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/cpp
export AS=/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/as
export AR=/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/ar
export RANLIB=/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/ranlib
export LDFLAGS="-L/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/IphoneOS5.0.sdk/usr/lib/system -L/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/IphoneOS5.0.sdk/usr/lib"

export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/Users/masterdipesh/Desktop/rifdish/librtmp/pkgconfig
./configure \
--cc=/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/gcc \
--as='./gas-preprocessor/gas-preprocessor.pl /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/gcc' \
--sysroot=/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS5.1.sdk \
--target-os=darwin --enable-pic \
--arch=arm \
--cpu=cortex-a8 \
--extra-cflags='-arch armv7 -I/Users/masterdipesh/Desktop/rifdish/librtmp/include' \
--extra-ldflags='-arch armv7 -lz -L/Users/masterdipesh/Desktop/rifdish/librtmp -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS5.1.sdk' \
--extra-libs='/Users/masterdipesh/Desktop/rifdish/librtmp/lib/librtmp.a /Users/masterdipesh/Desktop/rifdish/Binaries/iPhoneOS-V7-4.3/lib/libssl.a /Users/masterdipesh/Desktop/rifdish/Binaries/iPhoneOS-V7-4.3/lib/libcrypto.a' \
--prefix=compiled/armv7 \
--enable-librtmp \
--enable-cross-compile \
--enable-filters \
--disable-armv5te \
--disable-swscale-alpha \
--disable-doc \
--disable-ffmpeg \
--disable-ffplay \
--disable-ffprobe \
--disable-ffserver \
--disable-asm \
--disable-debug \
--disable-shared \
--enable-static \
--disable-mmx  \
--disable-neon \
--disable-armvfp \
--enable-version3 


make
make install
