#!/bin/sh

# 初期設定
WORK=$HOME/Builds/build-essential
PREFIX=$HOME/local
export PATH="$PREFIX/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin"

# ソースコードのダウンロード
if [ ! -d $WORK/src ] ; then
    mkdir src
fi
cd $WORK/src
curl -O ftp://ftp.gnu.org/gnu/m4/m4-1.4.16.tar.bz2
curl -O ftp://ftp.gnu.org/gnu/autoconf/autoconf-2.68.tar.bz2
curl -O ftp://ftp.gnu.org/gnu/automake/automake-1.11.1.tar.bz2
curl -O ftp://ftp.gnu.org/gnu/libtool/libtool-2.4.tar.gz

cd $WORK

# m4
tar xjf src/m4-*
cd m4-*
./configure\
    --prefix=$PREFIX
make clean && make -j4 && make install-strip
cd $WORK
rm -fr m4-*

# autoconf
tar xjf src/autoconf-2*
cd autoconf-2*
./configure\
    --prefix=$PREFIX\
    --disable-debug
make clean && make -j4 && make install-strip
cd $WORK
rm -fr autoconf-2*

# automake
tar xjf src/automake-*
cd automake-*
./configure\
    --prefix=$PREFIX
    --disable-debug
make clean && make -j4 && make install-strip
cd $WORK
rm -fr automake-*

# libtool
tar xjf src/libtool-*
cd libtool-*
./configure\
    --prefix=$PREFIX
make clean && make -j4 && make install-strip
cd $WORK
rm -fr libtool-*