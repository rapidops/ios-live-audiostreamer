AudioStreamer README
--------------------

AudioStreamer is a project, created to help the iOS developers to stream the audio to Wowza Server easily without any worries.

This project does streaming of live audio to the Wowwza Media Server.

AudioStreamer uses ffmpeg-1.0 for the audio processing, and includes compiled binary of ffmpeg library.

'AudioStreamer' folder includes full source code for AudioStreamer project.

'ffmpeg-1.0' folder includes source code for ffmpeg library. You can get more information about ffmpeg from it's website www.ffmpeg.org.