//
//  AudioStreamer.h
//  AudioStreamer
//
//  Created by Dipesh Patel on 26/03/13.
//  Copyright (c) 2013 RapidOps Solutions Pvt. Ltd. All rights reserved.
//
//  See the bottom of this file for licensing information.
//

#import <Foundation/Foundation.h>

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavformat/avio.h"

#include "libavfilter/avcodec.h"
#include "libavfilter/avfilter.h"
#include "libavfilter/avfiltergraph.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"

#include "Queue.h"

int isRunning;

@interface AudioStreamer : NSObject

/*!
 @abstract
 Initialize object of AudioStreamer
 
 @discussion
 Initializes and returns an object of AudioStreamer type.
 */
-(AudioStreamer *) init;

/*!
 @abstract
 Sets output url for boadcasting server.
 
 @discussion
 It sets url for broadcasting server where stream will be post.
 
 @param url a NSString
 */
-(void) setOutputURL: (NSString *) url;

/*!
 @abstract
 Starts streaming to given server.
 
 @discussion
 Starts streaming to given server with the recorded files available.
 */
-(void) startStreaming;

/*!
 @abstract
 Sets next file to process/stream.
 
 @discussion
 Append the file name next to process for stream.
 
 @param buffer a NSString
 */
-(void) appendToQueue: (NSString *)buffer;

/*!
 @abstract
 Stops stream broadcasting.
 
 @discussion
 Stops stream broadcasting and releases all memory using in broadcasting.
 */
-(void) stopStreaming;

/*!
 @abstract
 Starts broadcast for queued files.
 
 @discussion
 It starts broadcasting of queued files.
 
 Not to call directly, used internally only
 */
-(void) startProcessingQueue;

/*!
 @abstract
 Provides status information.
 
 @discussion
 Use this when want to know status of the broadcasting process,
 Broadcasting is running or not
 
 @return integer value 1 for RUNNING, 0 for NOT_RUNNING.
 */
-(int) isRunning;

@end

/*
 The MIT License
 
 Copyright (c) 2012 RapidOps Solutions Pvt. Ltd.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 */

