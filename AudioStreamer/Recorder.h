//
//  Recorder.h
//  AudioStreamer
//
//  Created by Dipesh Patel on 26/03/13.
//  Copyright (c) 2013 rapidops. All rights reserved.
//
//  See the bottom of this file for licensing information.
//

#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AudioToolbox/AudioQueue.h>
#import <AudioToolbox/AudioFile.h>

#include "curl.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavformat/avio.h"
#include "libavutil/avassert.h"
#include "libavformat/avio.h"
#include "libavutil/mathematics.h"
#include "libswscale/swscale.h"
#include "libavfilter/avfilter.h"
#include <libavfilter/avfiltergraph.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include "libavutil/opt.h"
#include "libavutil/fifo.h"

#define SAMPLE_RATE 44100.0

static const int kNumberBuffers = 3;                            // 1
OSStatus SetMagicCookieForFile (AudioQueueRef inQueue,
                                AudioFileID   inFile
                                );
typedef struct  {
	AudioStreamBasicDescription  mDataFormat;                   // 2
	AudioQueueRef                mQueue;                        // 3
	AudioQueueBufferRef          mBuffers[kNumberBuffers];      // 4
	AudioFileID                  mAudioFile;                    // 5
	UInt32                       bufferByteSize;                // 6
	SInt64                       mCurrentPacket;                // 7
	bool                         mIsRunning;                    // 8
	int curFileIdIndex;
} AQRecorderState;

@protocol RecorderDelegate <NSObject>
@optional
-(void) finishedFileWithPath:(NSString *)filePath;
@end

@interface Recorder : NSObject{
    
	NSTimer *timerRecord;
	
	int curFileIdIndex;
}

@property (nonatomic, weak) id <RecorderDelegate> delegate;

#pragma mark - Init and start-stop methods
- (id) initRecorderWithDelegate:(id)sender;
- (void) startRecording;
- (void) stopRecordingForOne;
- (void) stopRecording;

#pragma mark - Timer Methods
- (void) initTimer;
- (void) updateTimer:(NSTimer *)timer;

#pragma mark - Delegate Handler
- (void) sendFinishedFile;

@end

/*
 The MIT License
 
 Copyright (c) 2012 RapidOps Solutions Pvt. Ltd.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 */
