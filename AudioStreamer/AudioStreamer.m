//
//  AudioStreamer.m
//  AudioStreamer
//
//  Created by Dipesh Patel on 26/03/13.
//  Copyright (c) 2013 RapidOps Solutions Pvt. Ltd. All rights reserved.
//
//  See the bottom of this file for licensing information.
//

#import "AudioStreamer.h"

static AVFormatContext *fmt_ctx;
static AVCodecContext *dec_ctx;
AVFilterContext *buffersink_ctx;
AVFilterContext *buffersrc_ctx;
AVFilterGraph *filter_graph;
static int audio_stream_index = -1;
const char *filter_descr = "aformat=sample_fmts\=s16:sample_rates\=44100:channel_layouts\=stereo,aconvert=flt:stereo";

AVOutputFormat *out_fmt;
AVFormatContext *out_format_context;
AVStream *out_audio_st;
AVCodec *out_audio_codec;
int last_pts=0,size=0,last_size=0;
NSMutableArray *file_queue;

//int isRunning;
NSString *outputURL;

@implementation AudioStreamer

-(AudioStreamer *) init{
	file_queue = [[NSMutableArray alloc] init];
	init_avlib();
	outputURL=nil;
	return self;
}

-(void)setOutputURL: (NSString *) url{
	outputURL=url;
}

-(void)startStreaming{
	
	if(outputURL==nil){
		NSLog(@"No Output specified, please specify output url using setOutputURL function");
        //		exit(0);
		isRunning=0;
		return;
	}
	if(open_output([outputURL cStringUsingEncoding:NSASCIIStringEncoding]) <0){
		NSLog(@"Unable to open Output URL : %@",outputURL);
		isRunning=0;
        //		exit(0);
		return;
	}
	isRunning=1;
}

-(void)startProcessingQueue{
	if(isRunning==1){
		[self processQueue];
	}
}

-(void) appendToQueue: (NSString *)buffer{
	NSLog(@"Added New File to Queue");
    
	[file_queue enqueue:buffer];
}

-(void) processQueue{
	if(isRunning){
		NSString *filename = [file_queue dequeue];
		if(filename !=nil && filename != NULL){
			processInputFile([filename cStringUsingEncoding:NSASCIIStringEncoding]);
		}
		
        //		NSLog(@"---FILE : %@",filename);
		
        //		float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
        //		if (ver < 6.0) {
        //			[NSThread sleepForTimeInterval:0.15];
        //		}
		[NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(processQueue) userInfo:nil repeats:NO];
	}
	else{
		close_audio();
	}
}

-(void) stopStreaming{
	NSLog(@"Closed");
	isRunning=0;
}

static void init_avlib(){
	avcodec_register_all();
	avfilter_register_all();
	av_register_all();
	avformat_network_init();
}

static AVStream *add_audio_stream(AVFormatContext *oc, AVCodec **codec, enum AVCodecID codec_id)
{
	AVCodecContext *c;
	AVStream *st;
	
	/* find the audio encoder */
	*codec = avcodec_find_encoder(AV_CODEC_ID_AAC);
	if (!(*codec)) {
		fprintf(stderr, "Could not find codec\n");
        //		exit(1);
		isRunning=0;
		return NULL;
	}
	
	st = avformat_new_stream(oc, *codec);
	if (!st) {
		fprintf(stderr, "Could not allocate stream\n");
        //		exit(1);
		isRunning=0;
		return NULL;
	}
	st->id = 1;
	
	c = st->codec;
	
	/* put sample parameters */
	c->sample_fmt  = AV_SAMPLE_FMT_FLT;
	c->bit_rate    = 64000;
	c->sample_rate = 44100;
	c->channels    = 2;
	
	// some formats want stream headers to be separate
	if (oc->oformat->flags & AVFMT_GLOBALHEADER)
		c->flags |= CODEC_FLAG_GLOBAL_HEADER;
	
	return st;
}

static void open_audio(AVFormatContext *oc, AVCodec *codec, AVStream *st)
{
	AVCodecContext *c;
	
	c = st->codec;
	c->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;
	/* open it */
	if (avcodec_open2(c, codec, NULL) < 0) {
		fprintf(stderr, "could not open codec\n");
        //		exit(1);
		isRunning=0;
		return;
	}
}

static int open_output(const char *url){

	/* allocate the output media context */
	avformat_alloc_output_context2(&out_format_context, NULL, "rtsp", url);
	if (!out_format_context) {
		printf("Could not deduce output format from file extension: using MPEG.\n");
		avformat_alloc_output_context2(&out_format_context, NULL, "mpeg", url);
	}
	if (!out_format_context) {
		return -1;
	}
	out_fmt = out_format_context->oformat;
	out_audio_st = NULL;
	
	if (out_fmt->audio_codec != AV_CODEC_ID_NONE) {
		out_audio_st = add_audio_stream(out_format_context, &out_audio_codec, out_fmt->audio_codec);
	}
	if (out_audio_st)
		open_audio(out_format_context, out_audio_codec, out_audio_st);
	
	/* open the output file, if needed */
	if (!(out_fmt->flags & AVFMT_NOFILE)) {
		if (avio_open(&out_format_context->pb, url, AVIO_FLAG_WRITE) < 0) {
			fprintf(stderr, "Could not open '%s'\n", url);
			return -1;
		}
	}
	
	/* Write the stream header, if any. */
	if (avformat_write_header(out_format_context, NULL) < 0) {
		fprintf(stderr, "Error occurred when opening output file\n");
		return -1;
	}
	
	//av_dump_format(out_format_context, 0, url, 1);
	return 0;
}

static int open_input_file(const char *filename)
{
	int ret;
	AVCodec *dec;
	
	if ((ret = avformat_open_input(&fmt_ctx, filename, NULL, NULL)) < 0) {
		av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
		return ret;
	}
	
	if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
		av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
		return ret;
	}
	
	/* select the audio stream */
	ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_AUDIO, -1, -1, &dec, 0);
	if (ret < 0) {
		av_log(NULL, AV_LOG_ERROR, "Cannot find a audio stream in the input file\n");
		return ret;
	}
	audio_stream_index = ret;
	dec_ctx = fmt_ctx->streams[audio_stream_index]->codec;
	
	/* init the audio decoder */
	if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0) {
		av_log(NULL, AV_LOG_ERROR, "Cannot open audio decoder\n");
		return ret;
	}
	av_dump_format(fmt_ctx, 0, filename, 0);
	
	return 0;
}

static int init_filters(const char *filters_descr)
{
	char args[512];
	int ret;
	AVFilter *abuffersrc  = avfilter_get_by_name("abuffer");
	AVFilter *abuffersink = avfilter_get_by_name("ffabuffersink");
	AVFilterInOut *outputs = avfilter_inout_alloc();
	AVFilterInOut *inputs  = avfilter_inout_alloc();
	const enum AVSampleFormat sample_fmts[] = { AV_SAMPLE_FMT_S16, AV_SAMPLE_FMT_FLT, -1 };
	AVABufferSinkParams *abuffersink_params;
	const AVFilterLink *outlink;
	AVRational time_base = fmt_ctx->streams[audio_stream_index]->time_base;
	
	filter_graph = avfilter_graph_alloc();
	
	/* buffer audio source: the decoded frames from the decoder will be inserted here. */
	if (!dec_ctx->channel_layout)
		dec_ctx->channel_layout = av_get_default_channel_layout(dec_ctx->channels);
	snprintf(args, sizeof(args),
             "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%"PRIx64,
             time_base.num, time_base.den, dec_ctx->sample_rate,
             av_get_sample_fmt_name(dec_ctx->sample_fmt), dec_ctx->channel_layout);
	ret = avfilter_graph_create_filter(&buffersrc_ctx, abuffersrc, "in",
                                       args, NULL, filter_graph);
	
	if (ret < 0) {
		av_log(NULL, AV_LOG_ERROR, "Cannot create audio buffer source\n");
		return ret;
	}
	
	/* buffer audio sink: to terminate the filter chain. */
	abuffersink_params = av_abuffersink_params_alloc();
	abuffersink_params->sample_fmts     = sample_fmts;
	ret = avfilter_graph_create_filter(&buffersink_ctx, abuffersink, "out",
                                       NULL, abuffersink_params, filter_graph);
	av_free(abuffersink_params);
	if (ret < 0) {
		av_log(NULL, AV_LOG_ERROR, "Cannot create audio buffer sink\n");
		return ret;
	}
	
	/* Endpoints for the filter graph. */
	outputs->name       = av_strdup("in");
	outputs->filter_ctx = buffersrc_ctx;
	outputs->pad_idx    = 0;
	outputs->next       = NULL;
	
	inputs->name       = av_strdup("out");
	inputs->filter_ctx = buffersink_ctx;
	inputs->pad_idx    = 0;
	inputs->next       = NULL;
	
	if ((ret = avfilter_graph_parse(filter_graph, filters_descr,
                                    &inputs, &outputs, NULL)) < 0)
		return ret;
	
	if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
		return ret;
	
	/* Print summary of the sink buffer
	 * Note: args buffer is reused to store channel layout string */
	outlink = buffersink_ctx->inputs[0];
	av_get_channel_layout_string(args, sizeof(args), -1, outlink->channel_layout);
	av_log(NULL, AV_LOG_INFO, "Output: srate:%dHz fmt:%s chlayout:%s\n",
           (int)outlink->sample_rate,
           (char *)av_x_if_null(av_get_sample_fmt_name(outlink->format), "?"),
           args);
	
	avfilter_inout_free(&inputs);
	avfilter_inout_free(&outputs);
	
	return 0;
}

static void print_samplesref(AVFilterBufferRef *samplesref)
{
	const AVFilterBufferRefAudioProps *props = samplesref->audio;
	printf("Chanel Layout :%lld, Samples: %d, Sample Rate :%d\n",props->channel_layout,props->nb_samples,props->sample_rate);
}

static void processInputFile(const char *filename){
	
	if(open_input_file(filename)<0){
		printf("Unable to open Input URL : %s",filename);
		return;
	}
	
	NSLog(@"processInputFile() start");
	
	init_filters(filter_descr);
	AVCodecContext* codec_context = fmt_ctx->streams[audio_stream_index]->codec;
	out_format_context->bit_rate=fmt_ctx->bit_rate;
	
	AVPacket packet;
	int buffer_size = AVCODEC_MAX_AUDIO_FRAME_SIZE;
	AVFrame avf;
	int got_frame;
	int got_packet;
	int ret=0;
    
	while (1) {
        //		NSLog(@"------Enter main while-------");
		AVFilterBufferRef *samplesref;
        
		buffer_size = AVCODEC_MAX_AUDIO_FRAME_SIZE;
		if (av_read_frame(fmt_ctx, &packet) < 0) {
			break;	// End of stream. Done decoding.
		}
		avcodec_decode_audio4(codec_context, &avf, &got_frame, &packet);
		
		av_free_packet(&packet);
		
		//		Send the buffer contents to the audio device
		if(got_frame){
            //			NSLog(@"Enter If(got_frame)");
			if (av_buffersrc_add_frame(buffersrc_ctx, &avf, 0) < 0) {
				av_log(NULL, AV_LOG_ERROR, "Error while feeding the audio filtergraph\n");
				break;
			}
			
			/* pull filtered audio from the filtergraph */
			while (1) {
                //				NSLog(@"------Enter inner while-------");
				ret = av_buffersink_get_buffer_ref(buffersink_ctx, &samplesref, 0);
				if(ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
					break;
				if(ret < 0){
					isRunning=0;
					return; //exit(0);
				}
                
				if (samplesref) {
					//packet_queue_put(&samplesref);
                    //					NSLog(@"processInputFile() inner if (samplesref)");
					
					AVFrame *filtered_frame;
					filtered_frame = avcodec_alloc_frame();
					avcodec_get_frame_defaults(filtered_frame);
					int64_t frame_pts = AV_NOPTS_VALUE;
					
					if (samplesref->pts != AV_NOPTS_VALUE) {
						filtered_frame->pts = frame_pts = av_rescale_q(samplesref->pts,fmt_ctx->streams[0]->time_base, out_format_context->streams[0]->time_base) -
						av_rescale_q(1, AV_TIME_BASE_Q, out_format_context->streams[0]->time_base);
						
					}
					avfilter_copy_buf_props(filtered_frame, samplesref);
					filtered_frame->pts = frame_pts;
					
					AVPacket aac_packet;
					got_packet=0;
					av_init_packet(&aac_packet);
					size = avcodec_encode_audio2(out_format_context->streams[0]->codec, &aac_packet, filtered_frame, &got_packet);
					
                    //					NSLog(@"------Before Got packet-------");
					if(got_packet){
						
                        //						NSLog(@"------Got packet-------");
						
						aac_packet.pts=av_rescale_q(last_pts, fmt_ctx->streams[0]->time_base, out_format_context->streams[0]->time_base);
						aac_packet.dts=aac_packet.pts;
						aac_packet.pos=last_size;
						last_pts+=aac_packet.duration;
						last_size+=size;
						
						if (av_interleaved_write_frame(out_format_context, &aac_packet) != 0) {
							fprintf(stderr, "Error while writing audio frame\n");
                            //							exit(1);
							isRunning=0;
							return;
						}
                        //						av_free_packet(&aac_packet);
                        //						avfilter_unref_bufferp(&samplesref);
					}
					av_free_packet(&aac_packet);
					avcodec_free_frame(&filtered_frame);
                    //					NSLog(@"------After Got packet-------");
				}
                //				NSLog(@"------End inner while-------");
				avfilter_unref_bufferp(&samplesref); 
			}
		}
        //		NSLog(@"------End main while-------");
        //		avcodec_free_frame(&filtered_frame);
	}
	
	avfilter_graph_free(&filter_graph);
	if (dec_ctx)
		avcodec_close(dec_ctx);
	avformat_close_input(&fmt_ctx);
	
	NSError *error;
	if (![[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithUTF8String:filename] error:&error]) {
		NSLog(@"Error while removing file : %@",[error localizedDescription]);
	}
}

/********************************************* FILTER PACKET END *********************************/
/*
 * add an audio output stream
 */

static void close_audio()
{
	
	[NSThread sleepForTimeInterval:5];
	av_write_trailer(out_format_context);
	
	/* Close each codec. */
	if (out_audio_st)
		avcodec_close(out_audio_st->codec);
	
	/* Free the streams. */
	for (int i = 0; i < out_format_context->nb_streams; i++) {
		av_freep(&out_format_context->streams[i]->codec);
		av_freep(&out_format_context->streams[i]);
	}
	
	if (!(out_fmt->flags & AVFMT_NOFILE))
	/* Close the output file. */
		avio_close(out_format_context->pb);
	/* free the stream */
	av_free(out_format_context);
}

-(int) isRunning{
	return isRunning;
}

@end

/*
 The MIT License
 
 Copyright (c) 2012 RapidOps Solutions Pvt. Ltd.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 */
