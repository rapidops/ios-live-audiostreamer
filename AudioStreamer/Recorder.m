//
//  Recorder.m
//  AudioStreamer
//
//  Created by Dipesh Patel on 26/03/13.
//  Copyright (c) 2013 rapidops. All rights reserved.
//
//  See the bottom of this file for licensing information.
//

#import "Recorder.h"

@implementation Recorder
@synthesize delegate;

AQRecorderState aqData = {0};

#pragma mark - Init and start-stop methods
- (id) initRecorderWithDelegate:(id)sender{
	curFileIdIndex = 0;
    
	self.delegate = sender;
	
	return self;
}

- (void) startRecording{
	curFileIdIndex = 0;
	
	// Starts the timer and recording
	[self initTimer];
}

- (void) stopRecordingForOne{
    //	NSLog(@"Stopping Recording");
	
    // Change currentFileId at every 10 sec.
	aqData.curFileIdIndex = ++curFileIdIndex;
	
	// a codec may update its cookie at the end of an encoding session, so reapply it to the file now
	SetMagicCookieForFile(aqData.mQueue, aqData.mAudioFile);
	AudioFileClose(aqData.mAudioFile);
	aqData.mAudioFile=nil;
    
    //	NSLog(@"Recording Stopped");
	
	[self sendFinishedFile];
}

- (void) stopRecording{
	[self stopRecordingForOne];
	
	AudioQueueStop(aqData.mQueue, true);
	AudioQueueDispose(aqData.mQueue, true);
	
	[timerRecord invalidate];
}

#pragma mark - Delegate Handler
- (void) sendFinishedFile{
	if ([self.delegate respondsToSelector:@selector(finishedFileWithPath:)]) {
		[self.delegate finishedFileWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"rec%d.caf",curFileIdIndex-1 ]]];
	}
}

#pragma mark - Timer Methods
- (void) initTimer{
	timerRecord = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];
	
	[self setupAudioFormat:[NSString stringWithFormat:@"rec%d.caf",curFileIdIndex ]];
}

- (void) updateTimer:(NSTimer *)timer{
	
	// Close current file
	[self stopRecordingForOne];
	
	//[self setupAudioFormat:[NSString stringWithFormat:@"rec%d",curFileIdIndex ]];
	
	NSLog(@"File changed index : %i", curFileIdIndex);
}

#pragma mark - Recording Methods
static void HandleInputBuffer (void                                 *aqData,
                               AudioQueueRef                        inAQ,
                               AudioQueueBufferRef                  inBuffer,
                               const AudioTimeStamp                 *inStartTime,
                               UInt32                               inNumPackets,
                               const AudioStreamPacketDescription   *inPacketDesc)
{
    
	@autoreleasepool{  // 07-01-2013 yuvi
        
		AQRecorderState *pAqData = (AQRecorderState *) aqData;
		if (inNumPackets == 0 && pAqData->mDataFormat.mBytesPerPacket != 0)
			inNumPackets = inBuffer->mAudioDataByteSize / pAqData->mDataFormat.mBytesPerPacket;
		
		if(pAqData->mAudioFile==nil){
			
			// Create URL For given filename
			CFURLRef url = CFURLCreateWithString(kCFAllocatorDefault, (CFStringRef)[NSTemporaryDirectory() stringByAppendingString:[NSString stringWithFormat:@"rec%d.caf",pAqData->curFileIdIndex]], NULL);
			
			// create the audio file
			AudioFileCreateWithURL(url, kAudioFileCAFType, &pAqData->mDataFormat, kAudioFileFlags_EraseFile, &pAqData->mAudioFile);
			
			// Set magic cookies for all files
			SetMagicCookieForFile(pAqData->mQueue, pAqData->mAudioFile);
			pAqData->mCurrentPacket=0;
			
			CFRelease(url); // 28-12-2012 yuvi
		}
		
		OSStatus res = AudioFileWritePackets(pAqData->mAudioFile, FALSE, inBuffer->mAudioDataByteSize,inPacketDesc, pAqData->mCurrentPacket, &inNumPackets, inBuffer->mAudioData);
		
		if (res!=noErr) {
			NSLog(@"Error writing file");
		}
        
		pAqData->mCurrentPacket += inNumPackets;
		
		AudioQueueEnqueueBuffer (pAqData->mQueue,inBuffer,0,NULL);
		
	}	// 07-01-2013 yuvi
}

int ComputeRecordBufferSize(const AudioStreamBasicDescription *format, float seconds)
{
	int packets, frames, bytes = 0;
	frames = (int)ceil(seconds * format->mSampleRate);
	
	if (format->mBytesPerFrame > 0)
		bytes = frames * format->mBytesPerFrame;
	else {
		UInt32 maxPacketSize;
		if (format->mBytesPerPacket > 0)
			maxPacketSize = format->mBytesPerPacket;	// constant packet size
		else {
			UInt32 propertySize = sizeof(maxPacketSize);
			AudioQueueGetProperty(aqData.mQueue, kAudioQueueProperty_MaximumOutputPacketSize, &maxPacketSize,
								  &propertySize);
		}
		if (format->mFramesPerPacket > 0)
			packets = frames / format->mFramesPerPacket;
		else
			packets = frames;	// worst-case scenario: 1 frame in a packet
		if (packets == 0)		// sanity check
			packets = 1;
		bytes = packets * maxPacketSize;
	}
	return bytes;
}

OSStatus SetMagicCookieForFile (AudioQueueRef inQueue, AudioFileID   inFile) {
    OSStatus result = noErr;
    UInt32 cookieSize;
    if (AudioQueueGetPropertySize (inQueue,kAudioQueueProperty_MagicCookie,&cookieSize) == noErr) {
        char* magicCookie =(char *) malloc (cookieSize);
        if (AudioQueueGetProperty (inQueue,kAudioQueueProperty_MagicCookie,magicCookie,&cookieSize) == noErr)
            
            result =    AudioFileSetProperty (inFile,
                                              kAudioFilePropertyMagicCookieData,
                                              cookieSize,
                                              magicCookie
                                              );
        
        free (magicCookie);
    }
    return result;
}

- (void) setupAudioFormat:(NSString *)recordFile{
	//	AQRecorderState aqData = {0};
	
	aqData.mDataFormat.mFormatID = kAudioFormatLinearPCM;
	aqData.mDataFormat.mSampleRate = SAMPLE_RATE;
	aqData.mDataFormat.mChannelsPerFrame=2;
	
	UInt32 size = sizeof(aqData.mDataFormat.mSampleRate);
	
	aqData.mDataFormat.mBitsPerChannel = aqData.mDataFormat.mChannelsPerFrame*8;
	aqData.mDataFormat.mFramesPerPacket = 1;
	aqData.mDataFormat.mBytesPerFrame = aqData.mDataFormat.mChannelsPerFrame * sizeof(AudioSampleType);
	aqData.mDataFormat.mBytesPerPacket =aqData.mDataFormat.mFramesPerPacket * aqData.mDataFormat.mBytesPerFrame;
	
	aqData.mDataFormat.mReserved = 0;
	aqData.mDataFormat.mFormatFlags =kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
	
	AudioQueueNewInput (
						&aqData.mDataFormat,
						HandleInputBuffer,
						&aqData,
						NULL,
						kCFRunLoopCommonModes,
						0,
						&aqData.mQueue
						);
	
	size=sizeof(aqData.mDataFormat);
	AudioQueueGetProperty(aqData.mQueue, kAudioQueueProperty_StreamDescription,
						  &aqData.mDataFormat, &size);
	
	// Create URL For given filename
	CFURLRef url = CFURLCreateWithString(kCFAllocatorDefault, (CFStringRef)[NSTemporaryDirectory() stringByAppendingString:recordFile], NULL);
	
	// create the audio file
	AudioFileCreateWithURL(url, kAudioFileCAFType, &aqData.mDataFormat, kAudioFileFlags_EraseFile, &aqData.mAudioFile);
	
	// Set magic cookies for all files
	SetMagicCookieForFile(aqData.mQueue, aqData.mAudioFile);
	
	aqData.bufferByteSize = ComputeRecordBufferSize(&aqData.mDataFormat, 0.5);
	
	for (int i = 0; i < kNumberBuffers; ++i) {
        
        AudioQueueAllocateBuffer (
                                  aqData.mQueue,
                                  aqData.bufferByteSize,
                                  &aqData.mBuffers[i]
                                  );
        
        AudioQueueEnqueueBuffer (
								 aqData.mQueue,
                                 aqData.mBuffers[i],
                                 0,
                                 NULL
                                 );
    }
	
	CFRelease(url);	// 28-12-2012 yuvi
	
    aqData.mCurrentPacket = 0;
    aqData.mIsRunning = true;
    AudioQueueStart (aqData.mQueue,NULL);
}

@end

/*
 The MIT License
 
 Copyright (c) 2012 RapidOps Solutions Pvt. Ltd.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 */
